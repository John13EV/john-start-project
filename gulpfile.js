const gulp = require("gulp");
const { src, dest } = require("gulp");
const sass = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const gcmq = require("gulp-group-css-media-queries");

let paths = {
  sass: "assets/sass/*.scss",
};
let mainPath = {
  sass: "assets/sass/style.scss",
};

gulp.task("sass", () => {
  return src(mainPath.sass)
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(gcmq())
    .pipe(dest("assets/css/"));
});

// Rerun the task when a file changes
gulp.task("watch", () => {
  gulp.watch(paths.sass, gulp.series("sass"));
});
